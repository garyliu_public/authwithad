﻿using AuthWithAD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthWithAD.Service
{
    public interface IAuth
    {
        bool CheckByAD(AuthenticateModel model, out string errorMessage);
    }
}
