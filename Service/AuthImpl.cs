﻿using AuthWithAD.Models;
using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.DirectoryServices.Protocols;
using System.Net;

namespace AuthWithAD.Service
{
    public class AuthImpl:IAuth
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        public bool CheckByAD(AuthenticateModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                IConfiguration config = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
                string LDAPServer = config.GetValue<string>("LDAPServer");
                var ldi = new LdapDirectoryIdentifier(LDAPServer, 389);
                using (var ldapConnection = new LdapConnection(ldi))
                {
                    ldapConnection.AuthType = AuthType.Basic;
                    ldapConnection.SessionOptions.ProtocolVersion = 3;
                    var nc = new NetworkCredential(model.Username, model.Password);
                    try
                    {
                        ldapConnection.Bind(nc);
                        log.Info("LdapConnection authentication success");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.Message;
                        log.Error(errorMessage);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                log.Error(errorMessage);
                return false;
            }
        }
    }
}
