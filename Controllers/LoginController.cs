﻿using AuthWithAD.Models;
using AuthWithAD.Service;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System;

namespace AuthWithAD.Controllers
{
    public class LoginController : Controller
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        private readonly IAuth _auth;

        public LoginController(IAuth auth)
        {
            _auth = auth;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ReturnModel Get([FromBody] AuthenticateModel model)
        {
            string errorMessage = string.Empty;
            var ret = new ReturnModel();
            try
            {
                bool AuthOK = _auth.CheckByAD(model,out errorMessage);
                if (AuthOK)
                {
                    ret.Code = 200;
                    ret.Msg = "登入成功";
                    ret.Data = string.Format("{0}", model.Username);
                    return ret;
                }
                else
                {
                    ret.Code = 201;
                    ret.Msg = "帳號密碼錯誤";
                    return ret;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                ret.Code = 201;
                ret.Msg = "帳號密碼錯誤";
                return ret;
            }
        }
    }
}
